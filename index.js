"use strict"

let arr = [1, 2, 2, 3, 4, 4, 5];

let uniqueArray = (array) => [...new Set(array)];

console.log(uniqueArray(arr)); // [ 1, 2, 3, 4, 5 ]

